<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\{
            ConversationsController,
            LandingPageController,
            HomeController,
            AdminController,
            EquipmentsController,
            DoctorsController,
            ReviewsController
        };
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/',[LandingPageController::class, 'main']);
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::get('/article',[LandingPageController::class, 'article']);
Route::get('/doctors',[LandingPageController::class, 'doctors']);
Route::get('/doctors/{doctor}',[LandingPageController::class, 'doctor']);
Route::get('/equipment',[LandingPageController::class, 'equipment']);
Route::get('/services',[LandingPageController::class, 'services']);
Route::get('/services/{service}',[LandingPageController::class, 'service']);
Route::post('/',[LandingPageController::class, 'sendRequest']);


Route::get('/admin/main',[AdminController::class, 'main']);
Route::get('/admin/user',[AdminController::class, 'user_page']);


Route::get('/admin/equipment',[EquipmentsController::class, 'index']);
Route::post('/admin/equipment',[EquipmentsController::class, 'store']);
Route::get('/admin/equipment/create',[EquipmentsController::class, 'create']);
Route::get('/admin/equipment/{equipment}',[EquipmentsController::class, 'show']);
Route::get('/admin/equipment/destroy/{equipment}',[EquipmentsController::class, 'destroy']);
Route::get('/admin/equipment/edit/{equipment}',[EquipmentsController::class, 'edit']);
Route::put('/admin/equipment/{equipment}',[EquipmentsController::class, 'update']);


Route::get('/admin/doctor',[DoctorsController::class, 'index']);
Route::post('/admin/doctor',[DoctorsController::class, 'store']);
Route::get('/admin/doctor/create',[DoctorsController::class, 'create']);
Route::get('/admin/doctor/{doctor}',[DoctorsController::class, 'show']);
Route::get('/admin/doctor/destroy/{doctor}',[DoctorsController::class, 'destroy']);
Route::get('/admin/doctor/edit/{doctor}',[DoctorsController::class, 'edit']);
Route::put('/admin/doctor/{doctor}',[DoctorsController::class, 'update']);

Route::get('/admin/review',[ReviewsController::class, 'index']);
Route::post('/admin/review',[ReviewsController::class, 'store']);
Route::get('/admin/review/create',[ReviewsController::class, 'create']);
Route::get('/admin/review/{review}',[ReviewsController::class, 'show']);
Route::get('/admin/review/destroy/{review}',[ReviewsController::class, 'destroy']);
Route::get('/admin/review/edit/{review}',[ReviewsController::class, 'edit']);
Route::put('/admin/review/{review}',[ReviewsController::class, 'update']);


Route::get('conversations',[App\Http\Controllers\ConversationsController::class, 'index']);
Route::get('conversations/{conversation}',[App\Http\Controllers\ConversationsController::class, 'show']);