<?php

namespace Database\Factories;

use App\Models\Reply;
use App\Models\Conversation;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ReplyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Reply::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::where('id',rand(1,15))->first()->id,
            'conversation_id' => Conversation::where('id',rand(1,10))->first()->id,
            'body' => $this->faker->sentence,
        ];
    }
}
