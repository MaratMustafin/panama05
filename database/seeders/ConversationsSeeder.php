<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\{User,Conversation,Reply};

class ConversationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory(15)->create();
        Conversation::factory(10)->create();
        Reply::factory(15)->create();
    }
}
