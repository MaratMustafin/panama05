@extends('landing.layout')
@section('content')

<input id="lang" type="hidden" value="ru">
<div class="main-wrapper">
    @include('landing.include.header')

    <main class="main">


<!--Doctors-->
<section class="grey">
    <div class="container">
        <h3 class="title-primary">Наши доктора</h3>
        <div class="row row--multiline">
            <div class="col-md-4 col-sm-6">
                <a href="#" title="" class="doctor">
                    <div class="doctor__image">
                        <img src="/assets/img/doctors/asylbaeva.jpg" alt="">
                    </div>
                    <div class="doctor__desc">
                        <h4 class="doctor__name">Асылбаева Айнур Муханбедиевна</h4>
                        <div class="doctor__position">Врач ультразвуковой диагностики</div>
                    </div>
                </a>
            </div>
            <div class="col-md-4 col-sm-6">
                <a href="#" title="" class="doctor">
                    <div class="doctor__image">
                        <img src="/assets/img/doctors/kukanov.jpg" alt="">
                    </div>
                    <div class="doctor__desc">
                        <h4 class="doctor__name">Куканов Талгат Кайроллаевич</h4>
                        <div class="doctor__position">Спортивный врач</div>
                    </div>
                </a>
            </div>
            <div class="col-md-4 col-sm-6">
                <a href="#" title="" class="doctor">
                    <div class="doctor__image">
                        <img src="/assets/img/doctors/mukasheva.jpg" alt="">
                    </div>
                    <div class="doctor__desc">
                        <h4 class="doctor__name">Мукашева Гульнара Пернебаевна</h4>
                        <div class="doctor__position">Врач-кардиолог</div>
                    </div>
                </a>
            </div>
            <div class="col-md-4 col-sm-6">
                <a href="#" title="" class="doctor">
                    <div class="doctor__image">
                        <img src="/assets/img/doctors/asylbaeva.jpg" alt="">
                    </div>
                    <div class="doctor__desc">
                        <h4 class="doctor__name">Асылбаева Айнур Муханбедиевна</h4>
                        <div class="doctor__position">Врач ультразвуковой диагностики</div>
                    </div>
                </a>
            </div>
            <div class="col-md-4 col-sm-6">
                <a href="#" title="" class="doctor">
                    <div class="doctor__image">
                        <img src="/assets/img/doctors/kukanov.jpg" alt="">
                    </div>
                    <div class="doctor__desc">
                        <h4 class="doctor__name">Куканов Талгат Кайроллаевич</h4>
                        <div class="doctor__position">Спортивный врач</div>
                    </div>
                </a>
            </div>
            <div class="col-md-4 col-sm-6">
                <a href="#" title="" class="doctor">
                    <div class="doctor__image">
                        <img src="/assets/img/doctors/mukasheva.jpg" alt="">
                    </div>
                    <div class="doctor__desc">
                        <h4 class="doctor__name">Мукашева Гульнара Пернебаевна</h4>
                        <div class="doctor__position">Врач-кардиолог</div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>

</main>

@include('landing.include.footer')
</div>




@include('landing.include.modal')


@endsection