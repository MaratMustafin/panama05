@extends('landing.layout')
@section('content')

<input id="lang" type="hidden" value="ru">
<div class="main-wrapper">
    @include('landing.include.header')

    <main class="main">


<section>
    <div class="container container--sm">
        <ul class="breadcrumbs">
            <li><a href="/" title="Главная">Главная</a></li>
            <li><span>Оборудование</span></li>
        </ul>
        <article class="article">
            <h1 class="title-primary">Оборудование</h1>
            <p class="annotation">
                Современный Центр спортивной медицины, реабилитации и подготовки «PROSPORT» благодаря новейшему техническому оснащению и широкому спектру услуг не имеет аналогов в Казахстане.
            </p>
            <div class="equipment-preview">
                <a href="/assets/img/gallery/SSE_4499.jpg" title="" data-fancybox="galery"><img src="/assets/img/gallery/SSE_4499.jpg" alt=""></a>
                <a href="/assets/img/gallery/SSE_4502.jpg" title="" data-fancybox="galery"><img src="/assets/img/gallery/SSE_4502.jpg" alt=""></a>
                <a href="/assets/img/gallery/SSE_4505.jpg" title="" data-fancybox="galery"><img src="/assets/img/gallery/SSE_4505.jpg" alt=""></a>
                <a href="/assets/img/gallery/SSE_4510.jpg" title="" data-fancybox="galery"><img src="/assets/img/gallery/SSE_4510.jpg" alt=""></a>
                <a href="/assets/img/gallery/SSE_4514.jpg" title="" data-fancybox="galery"><img src="/assets/img/gallery/SSE_4514.jpg" alt=""></a>
                <a href="/assets/img/gallery/SSE_4517.jpg" title="" data-fancybox="galery"><img src="/assets/img/gallery/SSE_4517.jpg" alt=""></a>
                <a href="/assets/img/gallery/SSE_4520.jpg" title="" data-fancybox="galery"><img src="/assets/img/gallery/SSE_4520.jpg" alt=""></a>
            </div>
            <div class="carousel-regular equipment">
                <a href="javascript:;" title=""><img src="/assets/img/gallery/SSE_4499.jpg" alt=""></a>
                <a href="javascript:;" title=""><img src="/assets/img/gallery/SSE_4502.jpg" alt=""></a>
                <a href="javascript:;" title=""><img src="/assets/img/gallery/SSE_4505.jpg" alt=""></a>
                <a href="javascript:;" title=""><img src="/assets/img/gallery/SSE_4510.jpg" alt=""></a>
                <a href="javascript:;" title=""><img src="/assets/img/gallery/SSE_4514.jpg" alt=""></a>
                <a href="javascript:;" title=""><img src="/assets/img/gallery/SSE_4517.jpg" alt=""></a>
                <a href="javascript:;" title=""><img src="/assets/img/gallery/SSE_4520.jpg" alt=""></a>
            </div>
            <div class="text">
                <p>Мы предлагаем спортивный, медицинский и диагностический уход мирового класса как для членов олимпийской команды Казахстана, так и для людей, которые стремятся сохранить здоровье и активный образ жизни.</p>
                <p>Наша команда состоит из высококвалифицированных специалистов, которые обеспечивают индивидуальный и всесторонний подход к каждому клиенту.</p>
                <p>Благодаря команде из более чем 10 практикующих докторов мы предоставляем профилактические консультации, оценку, диагностику, лечение и реабилитацию, чтобы помочь вам вернуться к оптимальному состоянию здоровья, активности и производительности.</p>
                <p>Нашим ключевым приоритетом является сохранение лидерства в области спортивной медицины. Постоянное повышение квалификации наших врачей у лучших мировых практиков, расширение спектра услуг и забота о вашем здоровье – является нашей главной задачей.</p>
                <p>Мы расположены на олимпийской базе подготовки в среднегорье, что оценят и национальные сборные команды, и горожане, ищущие тишину и умиротворение вдали от городской суеты.</p>
            </div>
        </article>
        <script src="https://yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
        <script src="https://yastatic.net/share2/share.js"></script>
        <div class="ya-share2" data-services="vkontakte,facebook,twitter,whatsapp,telegram"></div>
    </div>
</section>

</main>

@include('landing.include.footer')
</div>



@section('javascript-include')
<script>
    $('.equipment').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        swipe: false,
        asNavFor: ".equipment-preview",
        swipeToSlide: true,
        prevArrow: '<i class="icon-left"></i>',
        focusOnSelect: true,
        nextArrow: '<i class="icon-right"></i>',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2
                }
            }
        ]
    });
    $('.equipment-preview').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        swipe: false,
        asNavFor: ".equipment",
        fade: true,
        arrows: false
    });
</script>
@endsection


@include('landing.include.modal')



@endsection