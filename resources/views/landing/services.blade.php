@extends('landing.layout')
@section('content')
<input id="lang" type="hidden" value="ru">
<div class="main-wrapper">
    @include('landing.include.header')

    <main class="main">


<section id="services" class="grey padding-bottom-none">
    <div class="container">
        <ul class="breadcrumbs">
            <li><a href="/" title="Главная">Главная</a></li>
            <li><span>Услуги</span></li>
        </ul>
        <h1 class="title-primary">Услуги</h1>
        <div class="spoilers services">
            <div class="spoiler">
                <div class="spoiler__title"><span>Консультации</span></div>
                <div class="spoiler__desc">
                    <div class="row row--multiline">
                        <div class="col-xs-8 col-md-10">
                            <a href="#" title="Прием врача кардиолога">Прием врача кардиолога</a></div>
                        <div class="col-xs-4 col-md-2 text-right">7 000</div>
                    </div>
                    <div class="row row--multiline">
                        <div class="col-xs-8 col-md-10">
                            <a href="#" title="Прием врача травматолога-ортопеда">Прием врача травматолога-ортопеда</a>
                        </div>
                        <div class="col-xs-4 col-md-2 text-right">7 000</div>
                    </div>
                    <div class="row row--multiline">
                        <div class="col-xs-8 col-md-10">
                            <a href="#" title="Прием врача по спортивной медицине">Прием врача по спортивной медицине</a>
                        </div>
                        <div class="col-xs-4 col-md-2 text-right">5 000</div>
                    </div>
                    <div class="row row--multiline">
                        <div class="col-xs-8 col-md-10">
                            <a href="#" title="Прием врача физиотерапевта">Прием врача физиотерапевта</a></div>
                        <div class="col-xs-4 col-md-2 text-right">5 000</div>
                    </div>
                    <div class="row row--multiline">
                        <div class="col-xs-8 col-md-10"><a href="#" title="Прием психолога">Прием психолога</a></div>
                        <div class="col-xs-4 col-md-2 text-right">5 000</div>
                    </div>
                    <div class="row row--multiline">
                        <div class="col-xs-8 col-md-10"><a href="#" title="Прием реабилитолога">Прием реабилитолога</a>
                        </div>
                        <div class="col-xs-4 col-md-2 text-right">6 000</div>
                    </div>
                    <div class="row row--multiline">
                        <div class="col-xs-8 col-md-10"><a href="#" title="Прием диетолога">Прием диетолога</a></div>
                        <div class="col-xs-4 col-md-2 text-right">6 000</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="packages" class="grey padding-bottom-none">
    <div class="container">
        <h1 class="title-primary">Пакеты</h1>
        <div class="spoilers services">
            <div class="spoiler">
                <div class="spoiler__title"><span>ОЦЕНКА СОСТОЯНИЯ ЗДОРОВЬЯ</span></div>
                <div class="spoiler__desc">
                    <div class="row row--multiline">
                        <div class="col-xs-8 col-md-10">
                            <p><a href="#" title="Консультация спортивного врача">Консультация спортивного врача</a></p>
                            <p><a href="#" title="Консультация кардиолога">Консультация кардиолога</a></p>
                            <p><a href="#" title="Нагрузочное ЭКГ тестирование">Нагрузочное ЭКГ тестирование</a></p>
                            <p><a href="#" title="Электрокардиография">Электрокардиография</a></p>
                            <p><a href="#" title="Эхокардиография">Эхокардиография</a></p>
                            <p><a href="#" title="Плантоскопия">Плантоскопия</a></p>
                            <p><a href="#" title="3D анализ позвоночника">3D анализ позвоночника</a></p>
                        </div>
                        <div class="col-xs-4 col-md-2 text-right">38 000</div>
                    </div>
                </div>
            </div>
            <div class="spoiler">
                <div class="spoiler__title"><span>ОЦЕНКА СОСТОЯНИЯ ЗДОРОВЬЯ</span></div>
                <div class="spoiler__desc">
                    <div class="row row--multiline">
                        <div class="col-xs-8 col-md-10">
                            <p><a href="#" title="Консультация спортивного врача">Консультация спортивного врача</a></p>
                            <p><a href="#" title="Консультация кардиолога">Консультация кардиолога</a></p>
                            <p><a href="#" title="Нагрузочное ЭКГ тестирование">Нагрузочное ЭКГ тестирование</a></p>
                            <p><a href="#" title="Электрокардиография">Электрокардиография</a></p>
                            <p><a href="#" title="Эхокардиография">Эхокардиография</a></p>
                            <p><a href="#" title="Плантоскопия">Плантоскопия</a></p>
                            <p><a href="#" title="3D анализ позвоночника">3D анализ позвоночника</a></p>
                        </div>
                        <div class="col-xs-4 col-md-2 text-right">38 000</div>
                    </div>
                </div>
            </div>
            <div class="spoiler">
                <div class="spoiler__title"><span>ОЦЕНКА СОСТОЯНИЯ ЗДОРОВЬЯ</span></div>
                <div class="spoiler__desc">
                    <div class="row row--multiline">
                        <div class="col-xs-8 col-md-10">
                            <p><a href="#" title="Консультация спортивного врача">Консультация спортивного врача</a></p>
                            <p><a href="#" title="Консультация кардиолога">Консультация кардиолога</a></p>
                            <p><a href="#" title="Нагрузочное ЭКГ тестирование">Нагрузочное ЭКГ тестирование</a></p>
                            <p><a href="#" title="Электрокардиография">Электрокардиография</a></p>
                            <p><a href="#" title="Эхокардиография">Эхокардиография</a></p>
                            <p><a href="#" title="Плантоскопия">Плантоскопия</a></p>
                            <p><a href="#" title="3D анализ позвоночника">3D анализ позвоночника</a></p>
                        </div>
                        <div class="col-xs-4 col-md-2 text-right">38 000</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="lab" class="grey">
    <div class="container">
        <h1 class="title-primary">Платные лабораторные услуги</h1>
        <div class="spoilers services">
            <div class="spoiler">
                <div class="spoiler__title"><span>БИОХИМИЧЕСКИЕ ИССЛЕДОВАНИЯ КРОВИ</span></div>
                <div class="spoiler__desc">
                    <div class="row row--multiline hidden-xs">
                        <div class="col-sm-6"><strong>Тест</strong></div>
                        <div class="col-sm-2"><strong>Материал</strong></div>
                        <div class="col-sm-2"><strong>Срок, дней</strong></div>
                        <div class="col-sm-2 text-right"><strong>Цена</strong></div>
                    </div>
                    <div class="row row--multiline">
                        <div data-title="Тест" class="col-sm-6"><a href="#" title="Альфа-1-антитрипсин">Альфа-1-антитрипсин</a></div>
                        <div data-title="Материал" class="col-sm-2">сыв.</div>
                        <div data-title="Срок, дней" class="col-sm-2">2</div>
                        <div data-title="Цена" class="col-sm-2 text-right">900</div>
                    </div>
                    <div class="row row--multiline">
                        <div data-title="Тест" class="col-sm-6"><a href="#" title="Аланинаминотрансфераза (АЛТ)">Аланинаминотрансфераза (АЛТ)</a></div>
                        <div data-title="Материал" class="col-sm-2">сыв.</div>
                        <div data-title="Срок, дней" class="col-sm-2">2</div>
                        <div data-title="Цена" class="col-sm-2 text-right">840</div>
                    </div>
                    <div class="row row--multiline">
                        <div data-title="Тест" class="col-sm-6"><a href="#" title="Аспартатаминотрансфераза (АСТ)">Аспартатаминотрансфераза (АСТ)</a></div>
                        <div data-title="Материал" class="col-sm-2">сыв.</div>
                        <div data-title="Срок, дней" class="col-sm-2">2</div>
                        <div data-title="Цена" class="col-sm-2 text-right">840</div>
                    </div>
                    <div class="row row--multiline">
                        <div data-title="Тест" class="col-sm-6"><a href="#" title="Щелочная фосфатаза (ЩФ)">Щелочная фосфатаза (ЩФ)</a></div>
                        <div data-title="Материал" class="col-sm-2">сыв.</div>
                        <div data-title="Срок, дней" class="col-sm-2">2</div>
                        <div data-title="Цена" class="col-sm-2 text-right">840</div>
                    </div>
                    <div class="row row--multiline">
                        <div data-title="Тест" class="col-sm-6"><a href="#" title="Гаммаглютамилтрансфераза (ГГТП)">Гаммаглютамилтрансфераза (ГГТП)</a></div>
                        <div data-title="Материал" class="col-sm-2">сыв.</div>
                        <div data-title="Срок, дней" class="col-sm-2">2</div>
                        <div data-title="Цена" class="col-sm-2 text-right">840</div>
                    </div>
                    <div class="row row--multiline">
                        <div data-title="Тест" class="col-sm-6"><a href="#" title="Лактатдегидрогеназа (ЛДГ)">Лактатдегидрогеназа (ЛДГ)</a></div>
                        <div data-title="Материал" class="col-sm-2">сыв.</div>
                        <div data-title="Срок, дней" class="col-sm-2">2</div>
                        <div data-title="Цена" class="col-sm-2 text-right">840</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

</main>
@include('landing.include.footer')
</div>




@include('landing.include.modal')


@endsection