<section class="grey">
    <div class="container">
        <h3 class="title-primary">Наши доктора</h3>
        <div class="carousel-regular doctors">
            <a href="#" title="" class="doctor">
                <div class="doctor__image">
                    <img src="/assets/img/doctors/asylbaeva.jpg" alt="">
                </div>
                <div class="doctor__desc">
                    <h4 class="doctor__name">Асылбаева Айнур Муханбедиевна</h4>
                    <div class="doctor__position">Врач ультразвуковой диагностики</div>
                </div>
            </a>
            <a href="#" title="" class="doctor">
                <div class="doctor__image">
                    <img src="/assets/img/doctors/kukanov.jpg" alt="">
                </div>
                <div class="doctor__desc">
                    <h4 class="doctor__name">Куканов Талгат Кайроллаевич</h4>
                    <div class="doctor__position">Спортивный врач</div>
                </div>
            </a>
            <a href="#" title="" class="doctor">
                <div class="doctor__image">
                    <img src="/assets/img/doctors/mukasheva.jpg" alt="">
                </div>
                <div class="doctor__desc">
                    <h4 class="doctor__name">Мукашева Гульнара Пернебаевна</h4>
                    <div class="doctor__position">Врач-кардиолог</div>
                </div>
            </a>
            <a href="#" title="" class="doctor">
                <div class="doctor__image">
                    <img src="/assets/img/doctors/asylbaeva.jpg" alt="">
                </div>
                <div class="doctor__desc">
                    <h4 class="doctor__name">Асылбаева Айнур Муханбедиевна</h4>
                    <div class="doctor__position">Врач ультразвуковой диагностики</div>
                </div>
            </a>
            <a href="#" title="" class="doctor">
                <div class="doctor__image">
                    <img src="/assets/img/doctors/kukanov.jpg" alt="">
                </div>
                <div class="doctor__desc">
                    <h4 class="doctor__name">Куканов Талгат Кайроллаевич</h4>
                    <div class="doctor__position">Спортивный врач</div>
                </div>
            </a>
            <a href="#" title="" class="doctor">
                <div class="doctor__image">
                    <img src="/assets/img/doctors/mukasheva.jpg" alt="">
                </div>
                <div class="doctor__desc">
                    <h4 class="doctor__name">Мукашева Гульнара Пернебаевна</h4>
                    <div class="doctor__position">Врач-кардиолог</div>
                </div>
            </a>
        </div>
    </div>
</section>