<section>
    <div class="container">
        <h3 class="title-primary">Оборудование</h3>
        <div class="carousel-regular equipment">
            <a href="/equipment.html" title=""><img src="/assets/img/gallery/SSE_4499.jpg" alt=""></a>
            <a href="/equipment.html" title=""><img src="/assets/img/gallery/SSE_4502.jpg" alt=""></a>
            <a href="/equipment.html" title=""><img src="/assets/img/gallery/SSE_4505.jpg" alt=""></a>
            <a href="/equipment.html" title=""><img src="/assets/img/gallery/SSE_4510.jpg" alt=""></a>
            <a href="/equipment.html" title=""><img src="/assets/img/gallery/SSE_4514.jpg" alt=""></a>
            <a href="/equipment.html" title=""><img src="/assets/img/gallery/SSE_4517.jpg" alt=""></a>
            <a href="/equipment.html" title=""><img src="/assets/img/gallery/SSE_4520.jpg" alt=""></a>
        </div>
    </div>
</section>