<header class="header">
    <div class="top">
        <div class="container">
            <a href="/" title="Главная" class="header__logo"><img src="/assets/img/logo.svg" alt=""></a>
            <div class="header__contacts">
                <a href="#roadMap" title="" data-fancybox="" class="contact">
                    <i class="icon-pin"></i>
                    <span>Схема проезда</span>
                </a>
                <div>
                    <a href="tel:+7-7273-31-51-52" title="" class="contact">
                        <i class="icon-phone"></i>
                        <span>+7-7273-31-51-52 (3130)</span>
                    </a>
                    <a href="tel:+7-702-403-88-36" title="" class="contact">
                        <i class="icon-whatsapp"></i>
                        <span>+7-702-403-88-36</span>
                    </a>
                </div>
            </div>
            <div class="mobile-menu">
                <a href="/login" title="Личный кабинет" class="ghost-btn">Личный кабинет</a>
                <nav class="menu hidden-md hidden-lg">
                    <ul>
                        <li><a href="/services" title="Услуги">Услуги</a></li>
                        <li><a href="/equipment" title="Оборудование">Оборудование</a></li>
                        <li class="dropdown">
                            <a href="javascript:;" title="О нас">О нас</a>
                            <ul>
                                <li><a href="/article" title="О центре">О центре</a></li>
                                <li><a href="/doctors" title="Наши доктора">Наши доктора</a></li>
                            </ul>
                        </li>
                        <li><a href="/contact" title="Контакты">Контакты</a></li>
                    </ul>
                </nav>
                <a href="#request" title="Оставить заявку" data-fancybox class="btn hidden-md hidden-lg">Оставить заявку</a>
                <div class="socials">
                    <a href="#" title="" target="_blank" class="icon-instagram"></a>
<!--                        <a href="#" title="" target="_blank" class="icon-vk"></a>-->
<!--                        <a href="#" title="" target="_blank" class="icon-twitter"></a>-->
                    <a href="#" title="" target="_blank" class="icon-facebook"></a>
                </div>
                <div class="language">
                    <a href="#" title="Русский" class="active">РУС</a><a href="#" title="Қазақша">ҚАЗ</a>
                </div>
            </div>
            <div class="menu-btn">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
    </div>
    <div class="bottom compensate-for-scrollbar">
        <div class="container">
            <nav class="menu">
                <ul>
                    <li><a href="/services" title="Услуги">Услуги</a></li>
                    <li><a href="/equipment" title="Оборудование">Оборудование</a></li>
                    <li class="dropdown">
                        <a href="javascript:;" title="О нас">О нас</a>
                        <ul>
                            <li><a href="/article" title="О центре">О центре</a></li>
                            <li><a href="/doctors" title="Наши доктора">Наши доктора</a></li>
                        </ul>
                    </li>
                    <li><a href="/contact" title="Контакты">Контакты</a></li>
                </ul>
            </nav>
            <a href="#request" title="Оставить заявку" data-fancybox class="ghost-btn ghost-btn--white">Оставить заявку</a>
        </div>
    </div>
</header>