<footer class="footer">
    <div class="container">
        <div class="row row--multiline">
            <div class="col-md-2">
                <img class="footer__logo" src="/assets/img/logo.svg" alt="">
            </div>
            <div class="col-md-5">
                <a href="#roadMap" title="" data-fancybox="" class="contact">
                    <i class="icon-pin"></i>
                    <span>Республика Казахстан, Алматинская область, Талгарский район, Алатауский сельский округ, село Рыскулово, Всемирная Академия бокса</span>
                </a>
                <a href="tel:+7-7273-31-51-52" title="" class="contact">
                    <i class="icon-phone"></i>
                    <span>+7-7273-31-51-52 (3130)</span>
                </a>
                <a href="tel:+7-702-403-88-36" title="" class="contact">
                    <i class="icon-whatsapp"></i>
                    <span>+7-702-403-88-36</span>
                </a>
                <div class="socials">
                    <a href="#" title="" target="_blank" class="icon-instagram"></a>
                    <a href="#" title="" target="_blank" class="icon-vk"></a>
                    <a href="#" title="" target="_blank" class="icon-twitter"></a>
                    <a href="#" title="" target="_blank" class="icon-facebook"></a>
                </div>
            </div>
            <div class="col-md-5">
                <nav class="sitemap">
                    <ul>
                        <li><a href="/services" title="Услуги">Услуги</a></li>
                        <li><a href="/equipment" title="Пакеты">Оборудование</a></li>
                        <li><a href="/article" title="О центре">О центре</a></li>
                        <li><a href="/doctors" title="Наши доктора">Наши доктора</a></li>
                        <li><a href="/contact" title="Контакты">Контакты</a></li>
                        <li><a href="#" title="Личный кабинет врача">Личный кабинет врача</a></li>
                    </ul>
                </nav>
            </div>
        </div>
        <hr>
        <div class="copyright">2019, Современный Центр спортивной медицины, реабилитации и подготовки «PROSPORT»</div>
    </div>
</footer>