<div id="request" class="modal modal--auto" style="display:none;">
    <div class="text-center">
        <h3 class="title-primary">Заявка</h3>
        <hr>
        <div class="subtitle">
            Если у Вас возникли вопросы, отправьте заявку на обратный звонок и мы с Вами свяжемся в ближайшее время
        </div>
    </div>

    <form action="/" method="POST">
        @csrf
        <div class="input-group">
            <input style="@error('name') background-color: red @enderror" name="name" type="text" class="input-regular" placeholder="Ваше имя" required>
        </div>
        <div class="input-group">
            <input style="@error('phone') background-color: red @enderror" name="phone" type="tel" onfocus="$(this).inputmask('+7 999 999 99 99')" class="input-regular" placeholder="Номер телефона" required>
        </div>
        <div class="input-group">
            <input style="@error('email') background-color: red @enderror" name="email" type="email" class="input-regular" placeholder="Email" required>
        </div>
        <div class="text-center"><button type="submit" class="btn">Отправить</button></div>
    </form>
</div>