<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">
    <title>{{ $title ?? "Без названия" }}</title>

    <link rel="stylesheet" href="/assets/libs/fancybox/dist/jquery.fancybox.min.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="/assets/libs/slick-carousel/slick/slick.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="/assets/css/style.css" type="text/css" media="screen"/>

    <link rel="apple-touch-icon" sizes="57x57" href="/assets/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/assets/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/assets/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/assets/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/assets/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/assets/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/assets/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/assets/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/assets/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/assets/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/assets/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/favicon/favicon-16x16.png">
    <link rel="manifest" href="/assets/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/assets/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <meta name="title" content="Заголовок">
    <meta name="description" content="Описание">
    <meta name="keywords" content="">
    <meta property="og:type" content="article">
    <meta property="og:title" content="Заголовок">
    <meta property="og:description" content="Описание">
    <meta property="og:url" content="/">
    <meta property="og:image" content="/assets/img/og-image.jpg">
    <meta property="og:site_name" content="localhost:9876">
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="Заголовок">
    <meta property="twitter:description" content="Описание">
    <meta property="twitter:image" content="/assets/img/og-image.jpg">
    <meta property="twitter:site" content="">
    <meta property="twitter:creator" content="">
    <meta property="twitter:url" content="/">
    <meta property="author" content="">
    <meta name="relap-image" content="/assets/img/og-image.jpg">
    <meta name="relap-title" content="Заголовок">
    <meta name="relap-description" content=" ">

</head>
<body>
@yield('content')
<div class="scroll-up icon-up-material compensate-for-scrollbar"></div>
<script src="/assets/libs/jquery/dist/jquery.js"></script>
<script src="/assets/js/scripts.js"></script>
<script src="/assets/js/parallax.min.js"></script>
<script src="/assets/libs/maskedinput/maskedinput.js"></script>
<script src="/assets/libs/fancybox/dist/jquery.fancybox.min.js"></script>
<script src="/assets/libs/slick-carousel/slick/slick.js"></script>
@yield('javascript-include')
<div id="roadMap" style="display:none;">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d46465.04860766074!2d77.28510563416387!3d43.291949183630216!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x22011103cacc06be!2z0JDQutCw0LTQtdC80LjRjyDQsdC-0LrRgdCwIEFJQkE!5e0!3m2!1sru!2skz!4v1564389355656!5m2!1sru!2skz" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
</body>
</html>