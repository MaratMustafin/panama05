@extends('landing.layout')
@section('content')

<input id="lang" type="hidden" value="ru">
<div class="main-wrapper">
    @include('landing.include.header')

    <main class="main">


<section>
    <div class="container container--sm">
        <ul class="breadcrumbs">
            <li><a href="/" title="Главная">Главная</a></li>
            <li><span>Альфа-1-антитрипсин</span></li>
        </ul>
        <article class="article">
            <h1 class="title-primary">Альфа-1-антитрипсин</h1>
            <div class="row row--multiline">
                <div class="col-sm-4">
                    <div class="billet">
                        <strong>Материал:</strong> сыворотка
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="billet">
                        <strong>Срок, дней:</strong> 2
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="billet">
                        <strong>Цена:</strong> 900
                    </div>
                </div>
            </div>
            <br>
            <p class="annotation">
                Дефицит альфа-1-антитрипсина – это врожденная нехватка ингибитора легочных протеаз альфа-1-антитрипсина, которая приводит к усиленной, обусловленной протеазами, деструкции тканей легких и развитию эмфиземы у взрослых.
            </p>
            <div class="text">
                <p>Накопление в печени патологического альфа-1-антитрипсина может привести к развитию болезни печени как у детей, так и у взрослых. Сывороточный уровень альфа-1-антитрипсина 11 μмоль/л ( 80мг/дл) подтверждает диагноз. Лечение включает в себя отказ от курения, назначение бронходилататоров, раннее начало терапии при развитии инфекции и, в отдельных случаях, замещение альфа-1-антитрипсина. Тяжелое поражение печени может потребовать ее трансплантации. Прогноз основывается в основном на степени поражения легких.</p>
            </div>
        </article>
        <script src="https://yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
        <script src="https://yastatic.net/share2/share.js"></script>
        <div class="ya-share2" data-services="vkontakte,facebook,twitter,whatsapp,telegram"></div>
    </div>
</section>

</main>

@include('landing.include.footer')
</div>



@include('landing.include.modal')

@endsection