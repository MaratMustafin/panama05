@extends('landing.layout')
@section('content')

<input id="lang" type="hidden" value="ru">
<div class="main-wrapper">
    @include('landing.include.header')

    <main class="main">


<section>
    <div class="container container--md">
        <ul class="breadcrumbs">
            <li><a href="/" title="Главная">Главная</a></li>
            <li><a href="#" title="Наши доктора">Наши доктора</a></li>
            <li><span>Ким Галина Трофимовна</span></li>
        </ul>
        <article class="article">
            <h1 class="title-primary">Ким Галина Трофимовна</h1>
            <div class="row row--multiline">
                <div class="col-sm-4">
                    <img class="article-image" src="/assets/img/doctors/kim.png" alt="">
                </div>
                <div class="col-sm-8">
                    <div class="text">
                        <p>Ким Галина Трофимовна - Заместитель директора по организационно-методической работе.</p>
                        <p>Врач с большим опытом работы за спиной. Более 20 лет являлась главным врачем в ведущих медицинских центрах г.Алматы. Контроль за лечебной деятельностью, работа по развитию и расширению спектра медицинских услуг нашего медицинского центра находится под ее чутким контролем.</p>
                    </div>
                </div>
            </div>
        </article>
        <script src="https://yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
        <script src="https://yastatic.net/share2/share.js"></script>
        <div class="ya-share2" data-services="vkontakte,facebook,twitter,whatsapp,telegram"></div>
    </div>
</section>

</main>

@include('landing.include.footer')
</div>


@section('javascript-include')
<script>
    $('.doctors').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        swipe: false,
        swipeToSlide: true,
        prevArrow: '<i class="icon-left"></i>',
        nextArrow: '<i class="icon-right"></i>',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });

    $('.equipment').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        swipe: false,
        swipeToSlide: true,
        prevArrow: '<i class="icon-left"></i>',
        nextArrow: '<i class="icon-right"></i>',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2
                }
            }
        ]
    });

    $('.reviews').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        swipe: false,
        fade: true,
        swipeToSlide: true,
        prevArrow: '<i class="icon-left"></i>',
        nextArrow: '<i class="icon-right"></i>'
    });

    $('.partners').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        swipe: false,
        swipeToSlide: true,
        prevArrow: '<i class="icon-left"></i>',
        nextArrow: '<i class="icon-right"></i>',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2
                }
            }
        ]
    });
</script>
@endsection


@include('landing.include.modal')



@endsection