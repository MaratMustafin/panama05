@extends('landing.layout')
@section('content')
<input id="lang" type="hidden" value="ru">
<div class="main-wrapper">
    @include('landing.include.header')

    <main class="main">


<!--Opening-->
<section class="opening">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <h1 class="opening__title">Подходит чемпионам.<br/>
                    Подходит вам!</h1>
                @if (session('message'))
                <h2 style="background-color:red">{{ session('message') }}</h2>
                @endif
                <h2 class="opening__subtitle">Современный Центр спортивной медицины, реабилитации и подготовки «PROSPORT»</h2>
            </div>
        </div>
    </div>
    <div class="opening__bg parallax-window" data-speed="0.5" data-parallax="scroll" data-image-src="/assets/img/gallery/SSE_4879.jpg"></div>
</section>

<!--About-->
<section>
    <div class="side-ornament"></div>
    <div class="container">
        <h3 class="title-primary">Кто мы</h3>
        <div class="row row--multiline revert-xs">
            <div class="col-sm-6">
                <div class="plain-text">
                    <p>Современный Центр спортивной медицины, реабилитации и подготовки «PROSPORT»
                        благодаря новейшему техническому оснащению и широкому спектру услуг не имеет
                        аналогов в Казахстане. Мы предлагаем спортивный, медицинский и диагностический уход
                        мирового класса как для членов олимпийской команды Казахстана, так и для людей, которые стремятся сохранить здоровье и активный образ жизни.</p>
                </div>
                <a href="#" title="Подробнее" class="btn">Подробнее</a>
            </div>
            <div class="col-sm-6">
                <img src="/assets/img/about.png" alt="">
            </div>
        </div>
    </div>
</section>

<!--Advantages-->
<section class="grey">
    <div class="container">
        <div class="text-center">
            <h3 class="title-primary">Почему нас выбирают</h3>
            <hr>
            <div class="subtitle">
                Мы предлагаем мультидисциплинарный подход через комплексные командные решения
                для улучшения эффективности спортивной подготовки и вашего здоровья.
                Наша команда включает в себя спортивных врачей, физиотерапевтов, тренеров, диетолога,
                психологов, спортивных массажистов, лечебных массажистов и спортивных кардиологов.
            </div>
        </div>
        <div class="privileges">
            <div class="privilege">
                <img src="/assets/img/privileges/solution.svg" alt="" class="privilege__img">
                <div class="privilege__desc">
                    - Комплексное решение для улучшения эффективности спортивной подготовки и вашего
                    здоровья, благодаря индивидуальному и всестороннему подходу к каждому клиенту.
                </div>
            </div>
            <div class="privilege">
                <img src="/assets/img/privileges/growth.svg" alt="" class="privilege__img">
                <div class="privilege__desc">
                    - Профилактические консультации, оценку, диагностику, лечение и реабилитацию,
                    благодаря команде из более чем 10 практикующих докторов.
                </div>
            </div>
            <div class="privilege">
                <img src="/assets/img/privileges/teamwork.svg" alt="" class="privilege__img">
                <div class="privilege__desc">
                    - Командный подход к вашему индивидуальному плану подготовки с использованием
                    новейших технологий.
                </div>
            </div>
            <div class="privilege">
                <img src="/assets/img/privileges/location.svg" alt="" class="privilege__img">
                <div class="privilege__desc">
                    - Удобное расположение – Олимпийская база подготовки в среднегорье, где проходят
                    учебно-тренировочные сборы национальные олимпийские команды, и близость к СОК
                    «Акбулак», от которого для наших клиентов возможна организация шаттлов.
                </div>
            </div>
        </div>
    </div>
</section>

<!--Price-list-->
<section class="padding-bottom-none">
    <div class="container">
        <h3 class="title-primary">Услуги и пакеты</h3>
        <div class="row row--multiline">
            <div class="col-md-6">
                <a href="/services.html" title="" class="privilege privilege--bordered">
                    <img src="/assets/img/stethoscope.svg" alt="" class="privilege__img">
                    <div class="privilege__desc">
                        <h4 class="privilege__title">Прайс лист по услугам</h4>
                    </div>
                </a>
            </div>
            <div class="col-md-6">
                <a href="/services.html#packages" title="" class="privilege privilege--bordered">
                    <img src="/assets/img/first-aid-kit.svg" alt="" class="privilege__img">
                    <div class="privilege__desc">
                        <h4 class="privilege__title">Прайс лист по пакетам</h4>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>

<!--Functional test of athletes-->
<section>
    <div class="container">
        <h3 class="title-primary">Функциональное тестирование спортсменов</h3>
        <div class="row row--multiline">
            <div class="col-sm-6">
                <img src="/assets/img/gallery/SSE_4789.jpg" alt="">
            </div>
            <div class="col-sm-6">
                <div class="plain-text">
                    <p>Функциональное тестирование — это обследование для
                        здоровых людей, которое позволяет оценить индивидуальные
                        физические возможности организма, правильно построить
                        тренировочный процесс в зависимости от целей и оценить
                        достигнутый прогресс.</p>
                </div>
                <a href="#" title="Подробнее" class="btn">Подробнее</a>
            </div>
        </div>
    </div>
</section>

<!--Доктора-->
@include('landing.include.doctors_include')

<!--Оборудование-->
@include('landing.include.equipment_include')

<!--Отзывы-->
@include('landing.include.reviews_include')

<!--Partners-->
<section>
    <div class="container">
        <h3 class="title-primary text-center">Партнеры</h3>
        <div class="carousel-regular partners">
            <a href="#" title="" target="_blank"><img src="/assets/img/partners/olympic.png" alt=""></a>
            <a href="#" title="" target="_blank"><img src="/assets/img/partners/mediker.png" alt=""></a>
            <a href="#" title="" target="_blank"><img src="/assets/img/partners/medexa.png" alt=""></a>
            <a href="#" title="" target="_blank"><img src="/assets/img/partners/private.png" alt=""></a>
        </div>
    </div>
</section>

</main>

@include('landing.include.footer')
</div>

@section('javascript-include')
<script>
    $('.doctors').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        swipe: false,
        swipeToSlide: true,
        prevArrow: '<i class="icon-left"></i>',
        nextArrow: '<i class="icon-right"></i>',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });

    $('.equipment').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        swipe: false,
        swipeToSlide: true,
        prevArrow: '<i class="icon-left"></i>',
        nextArrow: '<i class="icon-right"></i>',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2
                }
            }
        ]
    });

    $('.reviews').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        swipe: false,
        fade: true,
        asNavFor: ".reviews-section__bg",
        swipeToSlide: true,
        prevArrow: '<i class="icon-left"></i>',
        nextArrow: '<i class="icon-right"></i>'
    });
    $('.reviews-section__bg').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        asNavFor: ".reviews",
        swipe: false,
        fade: true,
        arrows: false
    });

    $('.partners').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        swipe: false,
        swipeToSlide: true,
        prevArrow: '<i class="icon-left"></i>',
        nextArrow: '<i class="icon-right"></i>',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2
                }
            }
        ]
    });
</script>
@endsection

@include('landing.include.modal')



@endsection