<aside class="sidebar">
    <div class="sidebar__top hidden-sm hidden-xs">
        <a href="/admin/main" title="Главная" class="logo"><img src="/admin_assets/img/logo.svg" alt=""></a>
    </div>
    <div class="menu-wrapper">
        <ul class="menu">
            {{-- <li class="dropdown active">
                <a href="javascript:;" title="Архив"><i class="icon-archive"></i> Архив</a>
                <ul>
                    <li><a href="/" title="Список фондов">Список фондов</a></li>
                    <li><a href="#" title="Добавить" class="add">+Добавить</a></li>
                    <li><a href="#" title="Список дел">Список дел</a></li>
                    <li><a href="#" title="Добавить" class="add">+Добавить</a></li>
                </ul>
            </li> --}}
            <li class="dropdown">
                <a href="javascript:;" title="Отчеты"><i class="icon-reports"></i> Добавить</a>
                <ul>
                    <li><a href="/admin/equipment" title="Оборудование">Оборудование</a></li>
                    <li><a href="/admin/equipment/create" title="Добавить" class="add">+Добавить</a></li>
                    <li><a href="/admin/doctor" title="Доктора">Доктора</a></li>
                    <li><a href="/admin/doctor/create" title="Добавить" class="add">+Добавить</a></li>
                    <li><a href="/admin/review" title="Отзыв">Отзыв</a></li>
                    <li><a href="/admin/review/create" title="Добавить" class="add">+Добавить</a></li>
                </ul>
            </li>
            {{-- <li class="dropdown">
                <a href="javascript:;" title="Справочник"><i class="icon-directory"></i> Справочник</a>
                <ul>
                    <li><a href="#" title="Ссылка">Ссылка</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="javascript:;" title="Организации"><i class="icon-organizations"></i> Организации</a>
                <ul>
                    <li><a href="#" title="Ссылка">Ссылка</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="javascript:;" title="Пользователи"><i class="icon-users"></i> Пользователи</a>
                <ul>
                    <li><a href="#" title="Ссылка">Ссылка</a></li>
                    <li><a href="#" title="Ссылка">Ссылка</a></li>
                </ul>
            </li> --}}
        </ul>
    </div>
</aside>