<header class="header">
    <div class="container container-fluid">
        <a href="javascript:;" title="Свернуть/развернуть навигацию" class="menu-btn icon-menu"></a>
        <a href="/" title="Главная" class="logo hidden-md hidden-lg"><img src="/admin_assets/img/logo-blue.svg" alt=""></a>
        <div class="language hidden-sm hidden-xs">
            <a href="#" title="РУС" class="active">РУС</a>
            <a href="#" title="ENG">ENG</a>
        </div>
        <div class="header-dropdown account-nav">
            <div class="header-dropdown__title">
                <a href="/admin/user/"><span>Добро пожаловать, {{ Auth::user()->name }}!</span> <img src="/admin_assets/img/user.svg" alt=""> <i
                    class="icon-chevron-down"></i></a>
            </div>
            <div class="header-dropdown__desc">
                <ul>
                    <li class="language hidden-md hidden-lg"><a href="#" title="РУС" class="active">РУС</a><a href="#" title="ENG">ENG</a></li>
                    <li><a href="{{ route('home') }}" title="Выйти">{{ __('Выйти') }}</a></li>
                </ul>
            </div>
        </div>
    </div>
</header>