@extends('admin.layout')
@section('content')
<input id="lang" type="hidden" value="ru">
<div class="main-wrapper">
@include('admin.include.aside')
<div class="right-wrapper">
@include('admin.include.header')

    <main class="main">


<div class="container container-fluid">
    <div class="block">
        <h2 class="title-primary">Пользователь</h2>
        <div class="input-group ">
            <label for="organizations"
                   class="input-group__title">Организация <span
                    class="required">*</span></label>
            <select name="organizations[]" id="organizations" class="input-regular chosen"
                    data-placeholder=" ">
                <option value="" data-permissions="">-</option>
                <option value="29"
                        data-permissions="5,6,7">
                    Test
                </option>
                <option value="15"
                        data-permissions="1,4,6,7">
                    Арысский городской государственный архив
                </option>
                <option value="9"
                        data-permissions="5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36">
                    Государственное учреждение &quot;Управление цифровизации, оказания государственных услуг и архивов Туркестанской области&quot;
                </option>
                <option value="14"
                        data-permissions="2,3">
                    Государственный архив общественно-политической истории Туркестанской области
                </option>
                <option value="16"
                        data-permissions="6,7">
                    Государственный архив района Байдибек
                </option>
                <option value="12"
                        data-permissions="11,12">
                    Жетысайский региональный государственный архив
                </option>
                <option value="17"
                        data-permissions="15,16">
                    Казыгуртский районный государственный архив
                </option>
                <option value="18"
                        data-permissions="30,8">
                    Келесский районный государственный архив
                </option>
                <option value="13"
                        data-permissions="17,12">
                    Кентауский региональный государственный архив
                </option>
                <option value="19"
                        data-permissions="1,8">
                    Мактааральский районный государственный архив
                </option>
                <option value="20"
                        data-permissions="9,13">
                    Ордабасинский районный государственный архив
                </option>
                <option value="21"
                        data-permissions="1,9,8">
                    Отрарский районный государственный архив
                </option>
                <option value="22"
                        data-permissions="36,14,11">
                    Сайрамский районный государственный архив
                </option>
                <option value="10"
                        data-permissions="5,6,7,8">
                    Сарыагаш
                </option>
                <option value="23"
                        data-permissions="2,5,9">
                    Сарыагашский районный государственный архив
                </option>
                <option value="24"
                        data-permissions="4,8,5">
                    Сузакский районный государственный архив
                </option>
                <option value="25"
                        data-permissions="3,4,6">
                    Толебийский районный государственный архив
                </option>
                <option value="26"
                        data-permissions="7,8,9">
                    Туркестанский городской государственный архив
                </option>
                <option value="11"
                        data-permissions="9,10,11">
                    Туркестанский областной государственный архив
                </option>
                <option value="27"
                        data-permissions="11,12">
                    Тюлькубасский районный государственный архив
                </option>
                <option value="28"
                        data-permissions="5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31">
                    Шардаринский районный государственный архив
                </option>
                <option value="8"
                        data-permissions="13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36">
                    Шымкентский городской государственный архив
                </option>
            </select>
        </div>
    </div>

    <div class="block">
        <h2 class="title-secondary">Доступы пользователя</h2>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="5" type="checkbox">
                <span>Просмотр списка структурных подразделений</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="6" type="checkbox">
                <span>Создание записи структурного подразделения</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="7" type="checkbox">
                <span>Редактирование записи структурного подразделения</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="8" type="checkbox">
                <span>Удаление записи структурного подразделения</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="9" type="checkbox">
                <span>Просмотр списка пользователей</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="10" type="checkbox">
                <span>Создание записи пользователя</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="11" type="checkbox">
                <span>Редактирование записи пользователя</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="12" type="checkbox">
                <span>Удаление записи пользователя</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="13" type="checkbox">
                <span>Просмотр списка фондов</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="14" type="checkbox">
                <span>Создание записи фонда</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="15" type="checkbox">
                <span>Редактирование записи фонда</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="16" type="checkbox">
                <span>Удаление записи фонда</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="17" type="checkbox">
                <span>Просмотр списка дел</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="18" type="checkbox">
                <span>Создание записи дела</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="19" type="checkbox">
                <span>Редактирование записи дела</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="20" type="checkbox">
                <span>Удаление записи дела</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="21" type="checkbox">
                <span>Создание документа/решения</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="22" type="checkbox">
                <span>Редактирование документа/решения</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="23" type="checkbox">
                <span>Удаление документа/решения</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="24" type="checkbox">
                <span>Загрузка скан-образов</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="25" type="checkbox">
                <span>Поиск</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="26" type="checkbox">
                <span>Работа с секретными документами</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="27" type="checkbox">
                <span>Работа с публичными документами</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="28" type="checkbox">
                <span>Просмотр статистики фондов</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="29" type="checkbox">
                <span>Просмотр статистики организаций</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="30" type="checkbox">
                <span>Просмотр статистики пользователей</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="31" type="checkbox">
                <span>Просмотр списка записей справочника</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="32" type="checkbox">
                <span>Создание записи справочника</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="33" type="checkbox">
                <span>Редактирование записи справочника</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="34" type="checkbox">
                <span>Удаление записи справочника</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="35" type="checkbox">
                <span>Просмотр статистики справочников</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="36" type="checkbox">
                <span>Просмотр статистики дел</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="37" type="checkbox">
                <span>Просмотр списка отчетов</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="38" type="checkbox">
                <span>Просмотр отчета по статистике</span>
            </label>
        </div>
    </div>
</div>

</main>

<footer class="footer">
</footer>
</div></div>


@section('javascript-include')
<!--Only this page's scripts-->
<script>
  $('#organizations').change(function () {
    var option = $("option:selected", "#organizations"),
      permissions = $.map(option.attr("data-permissions").split(','), function(value){
        return +value;
      });

    $("[name='permissions[]']").each(function () {
      var input = $(this);
      if (permissions.includes(parseInt(input.val()))) {
        input.prop('checked', true);
      } else {
        input.prop('checked', false)
      }
    });
  })
</script>
<!---->
@endsection
@include('admin.include.modal')
<!--
<script>
    $.fancybox.open({
      src: "#message",
      touch: false
    })
</script>-->


@endsection