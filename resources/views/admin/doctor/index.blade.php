@extends('admin.layout')
@section('content')
<input id="lang" type="hidden" value="ru">
<div class="main-wrapper">
@include('admin.include.aside')
<div class="right-wrapper">
@include('admin.include.header')

    <main class="main">


<div class="container container-fluid">
    <div class="title-block">
        <div class="row row--multiline align-items-center">
            <div class="col-md-4">
                <h1 class="title-primary" style="margin-bottom: 0">Архив</h1>
            </div>
            <div class="col-md-8 text-right-md text-right-lg">
                <div class="flex-form">
                    <div>
                        <a href="/admin/doctor/create" title="Добавить оборудование" class="btn"><i class="icon-search"></i> <span>Добавить доктора</span></a>
                    </div>
                    <div>
                        <form class="input-button">
                            <input type="text" name="search" placeholder="Имя доктора" class="input-regular input-regular--solid" style="width: 282px;">
                            <button class="btn btn--green">Найти</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="block">
    <h2 class="title-secondary">Список докторов</h2>
    <table class="table records">
        <colgroup>
            <col span="1" style="width: 3%;">
            <col span="1" style="width: 20%;">
            <col span="1" style="width: 12%;">
            <col span="1" style="width: 20%;">
            <col span="1" style="width: 15%;">
            <col span="1" style="width: 15%;">
        </colgroup>
        <thead>
        <tr>
            <th>#</th>
            <th>Имя доктора</th>
            <th>Биография</th>
            <th>Дата создания</th>
            <th>Дата изменения</th>
            <th>Действия</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($doctors as $doctor)
        <tr>
            <td>{{ $doctor->id }}</td>
            <td>{{ $doctor->name }}</td>
            <td>{{ $doctor->biography }}</td>
            <td>{{ $doctor->created_at }}</td>
            <td>{{ $doctor->updated_at }}</td>
            <td>
                @can('edit_forum')
                <div class="action-buttons">
                    <a href="/admin/doctor/{{ $doctor->id }}" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                    <a href="/admin/doctor/edit/{{ $doctor->id }}" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                    <a href="/admin/doctor/destroy/{{ $doctor->id }}" title="Удалить" class="icon-btn icon-btn--pink icon-delete"></a>
                </div>
                @endcan
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>

    <div class="text-right">
        <ul class="pagination">
            <li class="previous_page disabled"><span><i class="icon-chevron-left"></i></span></li>
            <li class="active"><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li class="dots disabled"><span>...</span></li>
            <li><a href="#">498</a></li>
            <li><a href="#">499</a></li>
            <li class="next_page"><a href="#"><i class="icon-chevron-right"></i></a></li>
        </ul>
    </div>
</div>

  
</div>

</main>

<footer class="footer">
</footer>
</div></div>

<!--Only this page's scripts-->

<!---->

@include('admin.include.modal')

<!--
<script>
    $.fancybox.open({
      src: "#message",
      touch: false
    })
</script>-->

@endsection