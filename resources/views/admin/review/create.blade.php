@extends('admin.layout')
@section('content')
<input id="lang" type="hidden" value="ru">
<div class="main-wrapper">
@include('admin.include.aside')
<div class="right-wrapper">
@include('admin.include.header')

    <main class="main">


<div class="container container-fluid">
    {{-- <ul class="breadcrumbs">
        <li><a href="#" title="Архив">Архив</a></li>
        <li><span>Moore-Barton</span></li>
    </ul> --}}

    <form class="block" action="/admin/review" method="POST">
        @csrf
        <div class="tabs">
            {{-- <div class="mobile-dropdown">
                <div class="mobile-dropdown__title dynamic">Основные реквизиты</div>
                <div class="mobile-dropdown__desc">
                    <ul class="tabs-titles">
                        <li class="active"><a href="javascript:;" title="Основные реквизиты">Основные реквизиты</a></li>
                        <li><a href="javascript:;" title="Служебная информация">Служебная информация</a></li>
                        <li><a href="javascript:;" title="История изменения">История изменения</a></li>
                        <li><a href="javascript:;" title="Служебные">Служебные</a></li>
                        <li><a href="javascript:;" title="Дела">Дела</a></li>
                        <li><a href="javascript:;" title="История изменения фонда">История изменения фонда</a></li>
                    </ul>
                </div>
            </div> --}}
            <a href="/admin/user">На главную</a>
            <hr>
            <h1>Добавить отзыв</h1>
            <div class="tabs-contents">
                <div class="active">
                    <div class="input-group">
                        <label class="input-group__title">Имя ревьюера</label>
                        <input style="@error('name') background-color: red @enderror" type="text" name="name" class="input-regular">
                    </div>
                    <div class="input-group">
                        <label class="input-group__title">Должность</label>
                        <input style="@error('profession') background-color: red @enderror" type="text" name="profession" class="input-regular">
                    </div>
                    <div class="input-group">
                        <label class="input-group__title">Цитата</label>
                        <textarea style="@error('quote') background-color: red @enderror" name="quote" placeholder="Заполните поле: Цитата"
                                  class="input-regular"></textarea>
                    </div>
                    <div class="input-group">
                        <button type="submit" class="btn btn--green">Сохранить</button>
                    </div>
                </div>
        <hr>
            {{-- <div>
                <button class="btn btn--red">Удалить</button>
            </div> --}}
    </form>
</div>

</main>

<footer class="footer">
</footer>
</div></div>



@section('javascript-include')
<script>
  var uploaders = new Array();

  initUploaders = function (uploaders) {
    console.log("initUploaders()");
    $(".file-upload").each(function () {
      var el = $(this),
        button = el.attr("id") + "_uploader",
        progressBar = el.find('.progress-bar'),
        input = el.siblings('input');
      console.log("Init uploader id:" + el.attr("id"));
      var uploader = new plupload.Uploader({
        runtimes: 'gears,html5,flash,silverlight,browserplus',
        browse_button: button,
        drop_element: button,
        max_file_size: '25mb',
        url: 'https://flowdoc.panama.kz/ajaxUploadFile',
        flash_swf_url: '/assets/libs/plupload/js/Moxie.swf',
        silverlight_xap_url: '/assets/libs/plupload/js/Moxie.xap',
        filters: [
          {title: "Document files", extensions: "docx,doc,xls,xlsx,pdf,txt,ppt,pptx"}
        ],
        unique_names: true,
        multiple_queues: false,
        multi_selection: false
      });

      uploader.bind('FilesAdded', function (up, files) {
        progressBar.css({width: 0});
        el.removeClass('error').removeClass('success').addClass('disabled');
        uploader.start();
      });

      uploader.bind("UploadProgress", function (up, file) {
        progressBar.css({width: file.percent + "%"});
      });

      uploader.bind("FileUploaded", function (up, file, response) {
        var obj = $.parseJSON(response.response.replace(/^.*?({.*}).*?$/gi, "$1"));
        input.val(obj.location);
        el.removeClass('disabled').removeClass('error').addClass('success');
        up.refresh();
      });

      uploader.bind("Error", function (up, err) {
        progressBar.css({width: 0});
        el.removeClass('disabled').removeClass('success').addClass('error');
        up.refresh();
      });

      uploader.init();

      uploaders.push(uploader);
    });
  };

  initUploaders(uploaders);
</script>
@endsection

@include('admin.include.modal')

<!--
<script>
    $.fancybox.open({
      src: "#message",
      touch: false
    })
</script>-->

@endsection