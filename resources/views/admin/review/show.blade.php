@extends('admin.layout')
@section('content')
<input id="lang" type="hidden" value="ru">
<div class="main-wrapper">
@include('admin.include.aside')
<div class="right-wrapper">
@include('admin.include.header')
<main class="main">


    <div class="container container-fluid">
        <ul class="breadcrumbs">
            <li><span>{{ $review->name }}</span></li>
        </ul>
    
        <div class="fund-header">
            <div class="fund-header__left">
                <div class="fund-header__id">{{ $review->name }}</div>
                <div class="fund-header__id"><a href="/admin/review/create">Добавить еще +</a></div>
                <h1 class="fund-header__title">{{ $review->profession }}</h1>
            </div>
            <div class="fund-header__right">
                <div class="property">
                    <div class="property__title">Дата создания</div>
                    <div class="property__text">{{ $review->created_at }}</div>
                </div>
                <div class="property">
                    <div class="property__title">Дата изменения	</div>
                    <div class="property__text">{{ $review->updated_at }}</div>
                    <div class="property__text">Администратор Panama DC</div>
                </div>
                <div class="property">
                    <div class="property__title">Администратор</div>
                    <div class="property__text"> Panama DC</div>
                </div>
            </div>
        </div>
        
        <div class="block">
            <div class="tabs">
    
                <div class="mobile-dropdown">
                    <div>Цитата</div>
                </div>
                <div class="tabs-contents">
                    <div class="active">
                        <br/>
                    
                        <h4>{{ $review->quote }}</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    </main>
    
    <footer class="footer">
    </footer>
    </div></div>

    
    
@endsection