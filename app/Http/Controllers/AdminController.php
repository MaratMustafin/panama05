<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Request as Req;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function main()
    {
        $reqs = Req::all();
        return view('admin.index',compact(['reqs']));
    }
    public function user_page()
    {
        return view('admin.user');
    }
}
