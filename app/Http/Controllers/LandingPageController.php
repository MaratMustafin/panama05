<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactMe;
use App\Models\{Doctor,Service,Request as Req};

class LandingPageController extends Controller
{
    public function main() 
    {
        $title = "ProSport KZ";
        return view('landing.index',[
            'title' => $title
        ]);
    }
    public function article() 
    {
        return view('landing.article');
    }
    public function doctors() 
    {
        return view('landing.doctors');
    }
    public function doctor(Doctor $doctor) 
    {
        return view('landing.doctor');
    }
    public function equipment() 
    {
        return view('landing.equipment');
    }
    public function services() 
    {
        return view('landing.services');
    }
    public function service(Service $service) 
    {
        return view('landing.service');
    }
    public function sendRequest(Req $request)
    {
        $request->create(
            $this->validateReq()
        );
        Mail::to(request('email'))
            ->send(new ContactMe(request('name'),request('email'),request('phone')));
        return redirect('/')->with('message','Заявка отправлена');
    }
    public function validateReq()
    {
        return request()->validate([
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required'
        ]);
    }
    
}
